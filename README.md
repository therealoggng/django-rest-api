# DJANGO REST API 

Hi guys ! I got such an assignment "Implement an API service, for a simple personal task manager ".

My main work was to make a CRUD for the task and tags, filters by the date of creation and end, also filter for the status.

 Fields for tasks: 
* title
* description
* status
* creation_date
* finish_date

 Fields for tags:
* title
* creation_date

I also i made registration through token based autenticated.

Wrote unittests for models, views,urls.

Wrapped the project in Docker 

## Getting Started

To start this project, you must have Python3.

At the bottom you can see how to start the project locally.
### Prerequisites

You must open a terminal and select a directory and
you need to install a virtual environment with the command bellow
```
python3 -m virtualenv -p python3 myvenv
```
> Know without a virtual environment there will be many conflicts. 

Now let's activate a virtual environment
```
. myvenv/bin/activate
```

### Installing

* Our first episode is called *"Repository cloning"*
```
https://gitlab.com/therealoggng/django-rest-api.git
```
> If you have ssh key then clone on top

* Our second episode is called *"Installing dependencies"*
 
But first make sure that you are in the directory where our files are located

```
pip install -r requirements.txt
```

* The next episode is called *"Migrations"*
```
python3 manage.py makemigrations
```

Then
```
python3 manage.py migrate
```

* And our last episode is called *"Runserver"*
```
python3 manage.py runserver
```
> Dont't forget to create a super admin

## Note for Docker

> To start the project through docker you must configure the base and staticfiles, but still leave the command below to start the project
```
docker-compose build
```
and to run Docker in the background, enter this
```
docker-compose up -d
```
and to create a user to log in to the admin panel, do this
```
docker-compose exec web python manage.py createsuperuser
```

## Authors

~ Shirinbekov Kutman  - [GitHub](https://github.com/therealoggng)

My email address (simpleshirinbekov@gmail.com)

### ALL ENJOY PROGRAMMING

