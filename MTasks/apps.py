from django.apps import AppConfig


class MtasksConfig(AppConfig):
    name = 'MTasks'
