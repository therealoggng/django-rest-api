from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.validators import UniqueValidator
from .models import Task, Tag

User = get_user_model()

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'title', 'description', 'status', 'creation_date', 'finish_date')


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(max_length=30, required=True)
    password = serializers.CharField(min_length=8, write_only=True, required=True)
    password2 = serializers.CharField(min_length=8, write_only=True, required=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password', 'password2']

    def create(self, validated_data):
        if validated_data['password2'] == validated_data['password']:
            user = User.objects.create_user(email=validated_data['email'], password=validated_data['password'],
                                            username=validated_data['username'])
            return user
        raise serializers.ValidationError('Your passwords don\'t match')



class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'title', 'date')
















'''class TaskSerializer(seria:lizers.Serializer):
    title = serializers.CharField(max_length=120)
    description = serializers.CharField()
    status = serializers.IntegerField()
    creation_date = serializers.DateTimeField()
    finish_date = serializers.DateTimeField()

    def create(self, validated_date):
        return Task.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.status = validated_data.get('status', instance.status)
        instance.creation_date = validated_data.get('creation_date', instance.creation_date)
        instance.finish_date = validated_data.get('finish_date', instance.finish_date)

        instance.save()
        return instance


class TagSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=120)
    date = serializers.DateTimeField()

    def create(self, validated_data):
        return Tag.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.date = validated_data.get('date', instance.date)

        instance.save()
        return instance'''
