from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()

class Task(models.Model):
    TASK_STATUS = {
        (1, 'В процессе выполнения'),
        (2, 'В ожидании выполнения'),
        (3, 'Выполнение закончено'),
    }
    author = models.ForeignKey(User, blank=True, null=True, verbose_name='Author', on_delete=models.CASCADE)
    title = models.CharField(max_length=60, null=False, blank=False, verbose_name='Название')
    description = models.TextField(max_length=1000, null=False, blank=False, verbose_name='Описание')
    status = models.IntegerField(choices=TASK_STATUS, verbose_name='Статус', default=2)
    creation_date = models.DateTimeField(blank=False, auto_now_add=True, verbose_name='Дата создания')
    finish_date = models.DateTimeField(auto_now=False, blank=True, null=True, verbose_name='Дата завершения')
    tag = models.ManyToManyField('Tag', related_name="tasks", verbose_name="тэги для задач", )


    def __str__(self):
        return self.title


class Tag(models.Model):
    author = models.ForeignKey(User, blank=True, null=True, verbose_name='Author tag', on_delete=models.CASCADE)
    title = models.CharField(max_length=50, null=False, blank=False, verbose_name='Название')
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата тэга')


    def __str__(self):
        return self.title
