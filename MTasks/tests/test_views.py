#from rest_framework.test import APITestCase
from django.test import TestCase
from ..models import Task

class TaskViewSetTest(TestCase):
    def setUp(self):
        Task.objects.create(title="title test", description="description test", status=1)

    def test_view_url_exists_at_proper_location(self):
        response = self.client.get('/api/v1/tasks/')
        self.assertEqual(response.status_code, 200)
