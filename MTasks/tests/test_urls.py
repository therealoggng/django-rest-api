from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from MTasks.models import Task, Tag

class TaskTests(APITestCase):
    def setUp(self):
        Task.objects.create(title='Live match', description='Chelsea-Mun United')
        Task.objects.create(title='Run to finish ', description='Marathon 2020')

    def test_create_task(self):
        url = reverse('task_list')
        data = {'title': 'Buy chocolate', 'description': 'Need some chocolate'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Task.objects.get(id=3).title, 'Buy chocolate')

    def test_get_single_task(self):
        url = reverse('task_detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_tasks_list(self):
        url = reverse('task_list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class TagTests(APITestCase):
    def setUp(self):
        Task.objects.create(title='Football', description='Best game')
        Tag.objects.create(title='Fb')

    def test_create_tag(self):
        url = reverse('tag_list')
        data = {'title': 'chocolate', 'tasks': [1]}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Tag.objects.get(id=2).title, 'chocolate')

    def test_get_single_tag(self):
        url = reverse('tag_detail', args=[1])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_tags_list(self):
        url = reverse('tag_list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
                   
