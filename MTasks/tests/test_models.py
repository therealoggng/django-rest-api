from django.test import TestCase

from django.contrib.auth.models import User


from ..models import Task


class TaskTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        testuser = User.objects.create_user(
            username='testuser', password='test12345')
        testuser.save()

        test_task = Task.objects.create(
            author=testuser, title='Truwer', description='медали тебя шинкуют', status='1')

        test_task.save()


    def test_task_content(self):
        task = Task.objects.get(id=1)
        expected_author = f'{task.author}'
        expected_title = f'{task.title}'
        expected_description = f'{task.description}'
        expected_status = f'{task.status}'
        expected_creation_date = f'{task.creation_date}'
        self.assertEqual(expected_author, 'testuser')
        self.assertEqual(expected_title, 'Truwer')
        self.assertEqual(expected_description, 'медали тебя шинкуют')
        self.assertEqual(expected_status, '1')
