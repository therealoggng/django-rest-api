from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from .permissions import IsAuthorOrReadOnly #IsAuthenticatedOrReadOnlyI
from django.dispatch import receiver
from django.db.models.signals import post_save
from .models import Task, Tag
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser, AllowAny
from .serializers import TaskSerializer, TagSerializer, UserSerializer

User = get_user_model

class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthorOrReadOnly,)
    serializer_class = TaskSerializer
    lookup_field = 'id'
    queryset = Task.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('creation_date', 'finish_date', 'status', 'tag',)


class TagViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthorOrReadOnly,)
    serializer_class = TagSerializer
    lookup_field = 'id'
    queryset = Tag.objects.all()

class SignUpAPIView(APIView):

    permission_classes = (AllowAny,)
    model = User

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                data = serializer.data
                return Response(data, status=status.HTTP_201_CREATED)
        return Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


@receiver(post_save, sender=User)
def create_user_token(sender, instance, created, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_token(sender, instance, **kwargs):
    instance.auth_token.save()


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    lookup_field = 'id'




















'''class TaskView(APIView):
    def get(self, request):
        tasks = Task.objects.all()
        serializer = TaskSerializer(tasks, many=True)
        return Response({"tasks": serializer.data})

    def post(self, request):
        task = request.data.get('task')
        serializer = TaskSerializer(data=task)
        if serializer.is_valid(raise_exception=True):
            task_saved = serializer.save()
        return Response({"success": "Task '{}' created successfully".format(task_saved.title)})

    def put(self, request, pk):
        saved_task = get_object_or_404(Task.objects.all(), pk=pk)
        data = request.data.get('task')
        serializer = TaskSerializer(instance=saved_task, data=data, partial=True)

        if serializer.is_valid(raise_exception=True):
            task_saved = serializer.save()

        return Response({
            "success": "Task '{}' updated successfully".format(task_saved.title)
        })

    def delete(self, request, pk):
        task = get_object_or_404(Task.objects.all(), pk=pk)
        task.delete()
        return Response({
            "message": "Task with id '{}' has been deleted.".format(pk)
        }, status=204)


class TagView(APIView):
    def get(self, request):
        tags = Tag.objects.all()
        serializer = TagSerializer(tags, many=True)
        return Response({"tags": serializer.data})

    def post(self, request):
        tag = request.data.get('tag')
        serializer = TagSerializer(data=tag)
        if serializer.is_valid(raise_exception=True):
            tag_saved = serializer.save()
        return Response({"success": "Tag '{}' created successfully".format(tag_saved.title)})

    def put(self, request, pk):
        saved_tag = get_object_or_404(Tag.objects.all(), pk=pk)
        data = request.data.get('tag')
        serializer = TagSerializer(instance=saved_tag, data=data, partial=True)

        if serializer.is_valid(raise_exception=True):
            tag_saved = serializer.save()

        return Response({
            "success": "Tag '{}' updated successfully".format(tag_saved.title)
        })

    def delete(self, request, pk):
        tag = get_object_or_404(Tag.objects.all(), pk=pk)
        tag.delete()
        return Response({
            "message": "Tag with id '{}' has been deleted.".format(pk)
        }, status=204)'''
