from django.contrib import admin
from .models import Task, Tag


class TaskAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'status', 'creation_date', 'finish_date']

class TagAdmin(admin.ModelAdmin):
    list_display = ['title', 'date']

admin.site.register(Task, TaskAdmin)
admin.site.register(Tag, TagAdmin)
