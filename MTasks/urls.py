from django.urls import path

from .views import TaskViewSet, TagViewSet, UserViewSet, SignUpAPIView

urlpatterns = [
    path('tasks/', TaskViewSet.as_view({
        'get': 'list', 'post': 'create'}),
        name='task_list'),
    path('tasks/<int:id>/', TaskViewSet.as_view({
        'get': 'retrieve','delete': 'destroy',
        'put': 'update', 'patch': 'partial_update'}),
        name='task_detail'),
#Tags
    path('tags/', TagViewSet.as_view({'get': 'list', 'post': 'create'}),
        name='tag_list'),
    path('tags/<int:id>/', TagViewSet.as_view({
        'get': 'retrieve', 'delete': 'destroy', 'put': 'update',
        'patch': 'partial_update'}),
        name='tag_detail'),
#Users
    path('users/', UserViewSet.as_view({'get': 'list', 'post': 'create'}),
        name='user_list'),
    path('users/<int:id>/', UserViewSet.as_view({
        'get': 'retrieve', 'delete': 'destroy', 'put': 'update',
        'patch': 'partial_update'}),
        name='user_detail'),
    path('signup/', SignUpAPIView.as_view(), name='signup_url')

]
